from flask import Flask, abort, request
from flask_restful import Resource, Api
from flask import jsonify
from flask_cors import CORS, cross_origin
## logging
import logging
file_handler = logging.FileHandler('server.log')

from spam_nsfw_detector import *

app = Flask(__name__)
api = Api(app)

app.logger.addHandler(file_handler)
app.logger.setLevel(logging.INFO)
CORS(app)

@app.route('/v1/detect',methods=['POST'])
def detect_spam_nfsw():
    response = {
                "metadata" : None,
                "status": "success",
                "code": 200,
                "messages": [],
                "data": None
                }
    query_text = request.get_json(force=True)["text"]
    response_data={"Spam":SpamDetection.process(query_text), "NSFW":NSFWDetector.process(query_text)}
    response["data"] = response_data
    return jsonify(response)

if __name__ == '__main__':
    app.logger.info("Server Started ...")
    app.run(host="0.0.0.0", port="7000",debug=True)

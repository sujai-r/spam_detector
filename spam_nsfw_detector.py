import re
from constants import OBSENE_WORDS
from abc import ABC, abstractmethod


class Task(ABC):

    @abstractmethod
    def process(cls,query):
        raise NotImplementedError("Trying to call abstract method")


class SpamDetection(Task):
    INTERNATIONAL_PH = re.compile("\d{2}([- ]*)\d{10}")
    PHONE_NUMBER =re.compile("\d{10}")
    WEBSITE = re.compile(
        "(https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|www\.[a-zA-Z0-9][a-zA-Z0-9-]+"
        "[a-zA-Z0-9]\.[^\s]{2,}|https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9]+\.[^\s]{2,}|www\.[a-zA-Z0-9]+\.[^\s]{2,})")

    @classmethod
    def process(cls, query):
        if 'call' in query.lower():
            return True
        if cls.PHONE_NUMBER.search(query) or cls.INTERNATIONAL_PH.search(query):
            return True
#        if cls.WEBSITE.search(query):
#            return True
        return False


class NSFWDetector(Task):
    OBSENE_WORDS = OBSENE_WORDS

    @classmethod
    def process(cls, query):
        if len(set(query.split(" ")).intersection(cls.OBSENE_WORDS))>0:
            return True
        return False


# if __name__ == '__main__':
#     query=input()
#     print("SpamDetection:",SpamDetection.process(query))
#     print("NSFWDetector:",NSFWDetector.process(query))
